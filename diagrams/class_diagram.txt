@startuml
class Actor {
  name: String
  description: String
  location: Location
  type: individual|region|consumption council|production collective
}
class Good {
  name: String
  description: String
  type: thing|service|vehicle|space|transport|other
  expires_at: Date
  location: Location
  amount: Amount
}
class Need {
  fulfilled: Boolean
  repeating: frequency
}
class Resource {
  use: unlimited|time limited
  condition: String
  consumed: Boolean
}
class Location {
  start/center: geocoordinate
  destination: geocoordinate
  area: list of geocoordinates
}
class Availability {
  start: DateTime
  end: DateTime
  available: Boolean
}
class Amount {
  quantity: Float
  unit: String
}

Good <|-- Need
Good <|-- Resource

Good "1" -- "*" Availability
Actor "1" -- "*" Need : communicates >
Actor "1" -- "*" Actor : organized in >
Actor "1" -- "*" Resource : freely provides >

@enduml
