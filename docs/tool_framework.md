# A Tool Supporting Free and Solidary Societies and Transformation

I'm not saying this is the one and only tool, it's just "a" tool, that could help.

#### Meta

target-audience: software people and those interested in organizing

## Software Requirements for Free and Solidary Societies

Free and solidary societies as described above will need some tools to communicate needs, to get an overview over resources, to manage scarce resources, and to create decentralized plans for production and distribution based on the needs.
The decentral and bottom-up structure needs to be reflected in the system architecture.

1. There does not have to be one version of the software. Regional or local instances of the software should be compatible to an agreed-on standardized protocol, in the same way as the free and solidary societies agreed on a minimal framework. This protocol allows the communication of needs and resources across regions and across the federated instances of the software.
2. Not every need and resource will have to be documented in the software if they can be fulfilled without software interaction. However, scarce resources should be modeled in and distributed with the software to make their use transparent and fair.
3. Depending on the type of need it might not be modeled at every locality level of the software: Special individual needs like massage or hair cuts will be communicated and fulfilled locally. Special needs of individuals like a special wheelchair will be communicated by the individual and then pushed upwards to the next locality level with a wheelchair workshop. Basic needs like potatoes will be communicated for a region and then distributed locally - there is no need to model potato needs of individuals. Globally scarce resources like allowed CO2 emissions will be defined globally and then distributed to regions and local communities.
4. If the production of a good requires resources, the deduced need for those resources could be modeled as a function of a number of input resources in given quantities producing a number of output resources in given quantities. If modeled that way, a tree of production needs becomes visible. The detailed modeling of input resources as deduced needs will only be necessary if the input resources are not generally available, for larger scale production optimization purposes, and for special purpose just in time production processes. For default processes like bicycle or food production, it will be enough to for a production facility which knows they need to produce X new bicycles per month, to communicate a regularly repeating need for all input materials without modelling a dependency tree of input materials. If production only happens once a need is communicated and the tree of required input resources (deduced needs) is made available (via additional production processes that also might require not yet produced input resources...) the production of vital goods might be too slow.
5. Scarce resource distribution agreements should also be modeled with the software.

### Diagrams

Architecture:
![Architecture](diagrams/architecture.png)

A minimal class diagram fulfilling the requirements of the protocol:
![MVP Class Diagram](diagrams/class_diagram.png)

If people decided to model more in detail, including processes and actions, the class diagram could look like this:
![Class Diagram Processes](diagrams/class_diagram_advanced.png)


## Use in Free and Solidary Societies

Individuals are part of one consumption community and they might be part of one or multiple production collectives. Everyone is part of exactly one local region. There is a hierarchy of regions of various locality levels. As stated in the above section, needs and resources are modeled on various locality levels. If needs can't be fulfilled within a region, they are communicated to neighboring regions. In case of scarcity, a distribution committees on the locality level of the scarcity finds a production and distribution solution that works best for everyone involved. This could include solutions in which people from one region move to another region for a limited time to help with production or the invention of new machines or processes for easier fulfillment of the needs.

There might be individuals, communities, or societies who use the tools like the described software intensively modeling every need, resource, and production process in detail and there might be individuals, communities, or societies which live mostly self-contained and only use the tool in cases in which they depend on services from outside their community.

Historical data about needs of regions, can be a source of information for decentralized planning for future time periods.


## Use during the Transformation

The tool should be easy to use and applicable for many purposes. E.g. it should also be possible to print lists of needs and resources or print blank need/resource lists for offline purposes like camps or post-disaster situations.

The software can replace existing tools, becoming multi-purpose, showing off the various resources that are already available in communities and connecting neighborhoods. It can be used to
- give away things for free that are no longer used (e.g. books, furniture)
- share tools like drills, sewing machines
- share cars or rides
- allow people to use meeting or vacation places for a limited time
- announce events
- list free digital resources
- look for people willing to help with small services
- lost&found/searching notice board

### Which needs can be satisfied that are not met in capitalism?

- connecting with people in the neighborhood
- helping each other, feeling useful
- getting needs met without money
- celebrating what's available for free
- access to tools, knowledge, resources, vacation places, ...
- possibility to live with a low amount of money because the number of needs met within the tool increases and thus reduced stress and work pressure

### Why should groups be encouraged?

First, the software will probably be used mostly locally by individuals or small groups. After some time, the federation aspect can be implemented, allowing to view needs/resources from other regions/instances and encouraging individuals to be part of groups.

Groups should be encouraged because:
- They provide privacy: The system does not need to know how many potatoes (or any other good) you as an individual need. You should just be able to take things of basic need from your group storage space.
- Groups have the coordination resources to estimate how many kilograms of potatoes will be needed for a month and to talk to potato producers trying to figure out a way how they could fulfill that need.
- Bundling needs at group/regional level reduces bureaucracy.
- Part of the transition process is also to develop new thinking patterns which probably includes overcoming the idea of individuals competing against each other which was common in capitalism.

Of course, living as an individual will still be possible and should not be frowned upon.

### Regularly Repeating Needs and Securities

First, the community using the software will probably only be able to fulfill one-off-needs. However, it should encourage people to create resources and process that allow fulfilling regularly repeating needs of groups, like bakery collectives and farming collectives providing for bread and vegetables. Of course, at first, freely available bread and vegetables will be scarce resource and coordination committees should decide how they should be distributed in a way in which the producing groups can also meet their needs. When more and more of those regularly repeating needs can be fulfilled within communities of the free and solidary societies, more and more people can reduce the amount of work they need to do in the capialistic system and can trust on the community fulfilling most of their needs. Thus, the new structures will start to provide existential securities for those that have experienced existential fear in capitalism.

### Additional Use Cases during Transformation

In addition to being a tool that allows to list resources and fulfill needs, variations of the tool with additional modules could also be used to simulate and visualize how needs will be fulfilled after the transformation to reduce the transformation risks and the fear of the unkown.
