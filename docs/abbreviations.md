## Abbreviations

- **CC**: Coordination Committee
- **CCC**: Continental Coordination Committee
- **ConCo**: Consumption Council
- **FSoSo**: Free and Solidary Societies
- **LACC**: Language Area Coordination Committee
- **LCC**: Local Coordination Committee
- **MRCC**: Mega Regional Coordination Committee
- **PC**: Production Collective
- **PCC**: Planetary Coordination Committee
- **RCC**: Regional Coordination Committee
- **RNN**: Regularly Recurring Needs
- **SN**: Special Need
