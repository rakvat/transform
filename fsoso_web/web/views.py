from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from .models import Resource, Need

def index(request):
    resources = Resource.non_expired()[:10]
    needs = Need.non_expired()[:10]
    context = {
        'resources': resources,
        'needs': needs,
    }
    return render(request, 'web/index.html', context)

def resources(request):
    resources = Resource.non_expired()
    context = {
        'resources': resources,
    }
    return render(request, 'web/resources.html', context)

def resource(request, resource_id):
    resource = Resource.objects.get(pk=resource_id)
    context = {
        'resource': resource,
    }
    return render(request, 'web/resource.html', context)

def needs(request):
    needs = Need.non_expired()
    context = {
        'needs': needs,
    }
    return render(request, 'web/needs.html', context)

def need(request, need_id):
    need = Resource.objects.get(pk=need_id)
    context = {
        'need': need,
    }
    return render(request, 'web/need.html', context)
