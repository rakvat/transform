from django.urls import path

from . import views

app_name = 'web'
urlpatterns = [
    path('', views.index, name='index'),
    path('resources', views.resources, name='resources'),
    path('resources/<int:resource_id>', views.resource, name='resource'),
    path('needs', views.needs, name='needs'),
    path('needs/<int:need_id>', views.need, name='need'),
]
