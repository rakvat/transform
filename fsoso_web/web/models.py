from django.utils import timezone
from django.db import models
from django.db.models import Q


class Good(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    expires_at = models.DateTimeField('expires at', blank=True, null=True)
    # TODO type, location, amount

    def __str__(self):
        return self.name

    def is_expired(self):
        return self.expires_at < timezone.now()

    @classmethod
    def non_expired(cls):
        return cls.objects.filter(Q(expires_at__gte=timezone.now()) | Q(expires_at=None))

class Resource(Good):
    condition = models.TextField(blank=True, null=True)
    consumed = models.BooleanField()

class Need(Good):
    fulfilled = models.BooleanField()
