from django.contrib import admin

from .models import Resource, Need


class ResourceAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'expires_at', 'condition', 'consumed']

class NeedAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'expires_at', 'fulfilled']

admin.site.register(Resource, ResourceAdmin)
admin.site.register(Need, NeedAdmin)
