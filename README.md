# Transformation into Free and Solidary Societies

## Documentation

- *Read this if you are interested in politics*: [Transformation Theory](docs/transform_theory.md) - **Why** most current societies suck and **how** we can transform into free and solidary societies.
- *Read this if you are interested in software.*: [Tool Framework](docs/tool_framework.md) - How federated instances of software communicating over protocols can help with the transformation.

## Tech Setup


### Django app

```
pipenv shell
python manage.py runserver
```

### Diagrams

The diagrams are generated with [PlantUML](http://plantuml.com):
```
cd diagrams && java -jar plantuml.jar *.txt
```
